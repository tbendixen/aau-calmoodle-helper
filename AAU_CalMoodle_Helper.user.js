// ==UserScript==
// @name         AAU CalMoodle Helper
// @namespace    http://www.blazing-skies.com/
// @version      0.8.0
// @description  enter something useful
// @match        http://www.moodle.aau.dk/calmoodle/public/*
// @match        https://www.moodle.aau.dk/calmoodle/public/*
// @copyright    2014+, Tristan Bendixen
// @require      http://code.jquery.com/jquery-latest.js
// @grant        GM_getValue
// @grant        GM_setValue
// ==/UserScript==

function setupAauCalMoodlehelper() {
  var settingsBlock =
    '<h2>AAU CalMoodle Helper settings</h2>'+
    '<div id="aaucmhsettings">'+
    '<input type="text" id="aaucmhnote" value="' + GM_getValue("aaucmhnote", "SW3;AD1;Semester Introduction;Project marked") + '" size="50" />'+
    '<button id="aaucmhsave">Save settings</button>'+
    '</div>';

  $("#kursustable").after(settingsBlock);
  
  $("#aaucmhsave").click(function() {
    var noteValue = $("#aaucmhnote").prop("value");

    GM_setValue("aaucmhnote", noteValue);
    dimElements();
  });

  dimElements();
}

function dimElements() {
  var noteString = GM_getValue("aaucmhnote", "SW3;AD1;Semester Introduction;Project marked");
  var noteStrings = noteString.split(";");

  // Make sure elements are reset
  resetElements();

  // Find elements to keep
  var elementsToKeep = null;
  if (noteStrings.length > 0) {
    elementsToKeep = $("div.note:contains('" + noteStrings[0] + "')").parent(".event");

    for (var i = 1; i < noteStrings.length; i++) {
      elementsToKeep = elementsToKeep.add($("div.note:contains('" + noteStrings[i] + "')").parent(".event"));
    }
  }

  // Find all the elements that need to be dimmed
  var elementsToDim = $("div.event").not(elementsToKeep);

  // Dim elements that needs dimming
  elementsToDim.each(function(index) {
    $(this).fadeTo("fast", 0.3);
  });
}

function resetElements() {
  $("div.event").each(function(index) {
    $(this).fadeTo("fast", 1.0);
  });
}

// Execute function
$(setupAauCalMoodlehelper());
