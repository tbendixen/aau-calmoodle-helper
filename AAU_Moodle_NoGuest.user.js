// ==UserScript==
// @name         Moodle NoGuest
// @namespace    http://www.blazing-skies.com/
// @version      0.2.1
// @description  Redirects to login, if Moodle detects a guest login.
// @match        https://www.moodle.aau.dk/*
// @match        http://www.moodle.aau.dk/*
// @copyright    2014+, Tristan Bendixen
// @require      http://code.jquery.com/jquery-latest.js
// ==/UserScript==

// Convenience method
$.fn.exists = function () {
    return this.length !== 0;
}


function getLogoutLink() {
    return $("a:contains('Logout')").attr("href");
}


function runAauMoodleNoGuest() {
    if ($("div:contains('guest@aau.dk')").exists()) {
        window.location.replace(getLogoutLink());
    }
}


$(runAauMoodleNoGuest());